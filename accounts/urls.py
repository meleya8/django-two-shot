from django import views
from django.urls import path
from django.contrib.auth import views as auth_views

from accounts.views import signup

urlpatterns = [
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("accounts/signup/", signup, name="signup"),
]
